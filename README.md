# Esempio applicazione con Vue Router

Esempio di applicazione realizzata con Vue Router
per la lezione 21 della guida a Vue.js disponibile su [Mr. Webmaster.it](https://www.mrwebmaster.it)

![screenshot applicazione realizzata con vue router](https://bitbucket.org/clau4io/vue-router-example/raw/79118fbed8a568c4b9d5a6d9b9d6f9987e1cccad/screenshots/vue-router-example-screenshot.png)

## Istruzioni

Dopo essersi assicurati di aver installato Node.js `node --version`, scaricare il repository.

```
git clone https://clau4io@bitbucket.org/clau4io/vue-router-example.git
```

Spostarsi nella nuova cartella ed eseguire il comando `npm install` per installare
tutte le dipendenze.

### Avviare server locale durante la fase di sviluppo

Lanciare il comando `npm start` per avviare il server locale e JSON server che è stato installato
localmente fra le dipendenze.

