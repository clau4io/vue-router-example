import Vue from 'vue';
import Vuex from 'vuex';

import RestaurantService from '@/services/RestaurantService';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    restaurants: []
  },
  mutations: {
    SET_RESTAURANTS(state, restaurants) {
      state.restaurants = restaurants;
    }
  },
  actions: {
    async getAllRestaurants({ commit }) {
      try {
        const response = await RestaurantService.getAllRestaurants();
        commit('SET_RESTAURANTS', response.data);
      } catch (error) {
        console.log(error);
      }
    }
  },
  getters: {
    getRestaurantBySlug: (state) => (slug) =>
      state.restaurants.find((restaurant) => restaurant.slug === slug),
    getRestaurantLocationById: (state) => ({ slug, id }) => {
      const restaurant = state.restaurants.find(
        (restaurant) => restaurant.slug === slug
      );
      const locations = restaurant.locations;
      return locations.find((location) => location.id === +id);
    }
  }
});
