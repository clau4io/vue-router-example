import axios from 'axios';

const { VUE_APP_REMOTE_ADDRESS: ADDRESS, VUE_APP_PORT: PORT } = process.env;

const axiosInstance = axios.create({
  baseURL: `//${ADDRESS}:${PORT}`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});

export default {
  getAllRestaurants() {
    return axiosInstance.get('/restaurants');
  },
  getRestaurant(id) {
    return axiosInstance.get(`/restaurants/${id}`);
  }
};
