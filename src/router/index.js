import Vue from 'vue';
import VueRouter from 'vue-router';
import Restaurants from '../views/Restaurants.vue';
import Restaurant from '../views/Restaurant.vue';
import RestaurantLocation from '../views/RestaurantLocation.vue';
import Error404 from '../views/404.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: { name: 'restaurants' }
  },
  {
    path: '/ristoranti',
    name: 'restaurants',
    component: Restaurants
  },
  {
    path: '/ristoranti/:slug',
    name: 'restaurant',
    props: true,
    component: Restaurant,
    children: [
      {
        path: ':id',
        props: true,
        name: 'restaurant-location',
        component: RestaurantLocation
      }
    ]
  },
  {
    path: '/chi-siamo',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '*',
    name: 'all',
    component: Error404
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'active',
  linkActiveClass: 'active',
  routes
});

export default router;
